# Quersumme
Programmiere eine Oberfläche, welche vier Eingabefelder beinhaltet, in welche der Benutzer jeweils eine Ziffer eingeben kann. Mit dem Klick auf den Button Quersumme wird die Summe der vier Felder berechnet. Ist die Summe gleich `9` wird neben der Summe auch `Anno ` + die vier Zahlen ausgegeben. Beispiel: `1`, `8`, `0`, `0` -> Ausgabe `Anno 1800`. Ansonsten wird nur die Quersumme ausgegeben, als zum Beispeil `13`.

# Hintergrund
https://de.wikipedia.org/wiki/Anno_(Spieleserie)#Trivia

## GUI
Eingabefeld `zahl1`
Eingabefeld `zahl2`
Eingabefeld `zahl3`
Eingabefeld `zahl4`
Button `Quersumme`

## Bedingungen
Verwende zur Implementierung die Funktion:
~~~
function isAnno()
~~~

### Wenn dies fertig ist:
Überprüft alle Eingaben auf Richtigkeit.
