## Aufgabe 6 (schwer): Befüllen à la Fibonacci.
Die Fibonacci-Reihe zählt immer das aktuelle und das vorherige Element zusammen und schreibt das Ergebnis in das nächste Feld.
Die Fibonacci Reihe beginnt immer mit `1,1`. Beispiel:

`1,1,2,3,5,8,...` - `1 + 1 = 2`; `1 + 2 = 3`; `2 +3 = 5`; usw...

**TIPP:** Die mindestlänge des Arrays muss immer 3 sein! Die ersten beiden Felder können immer mit `1` und `1` initialisiert werden. Schleifenstart bedenken!

Verwende für das Programm die Funktionen `fillFibo(myarray)` und `printFibo(myarray)`. Frage den Benutzer, wieviele Fibonacci Felder er haben möchte (>2).

-----

# Lösung:

~~~javascript
/**
 * File:    main.js
 * Project: 14_ja_while_schaltjahre
 * Author:  Johanna Kleinsasser
 * Date:    29.11.2018
 */

'use strict';

// functions
function fillFibo(laenge){
    
    let myFibo = [];

    // Die ersten beiden Felder bei Fibonacci sind immer 1!
    myFibo[0] = 1;
    myFibo[1] = 1;


    // Daher startet die Schleife bei 2, daher muss das Array mindestens eine Länge von 3 haben!
    for (let i = 2; i < laenge - 1; i++) {
        //neue Zahl berechnen...
        let neueFiboZahl = myFibo[i - 2]  + myFibo[i - 1];
        // und in das aktuelle Feld, beim start ist das das Feld 3 Feld mit der IndexZahl 2, schreiben
        myFibo[i] = neueFiboZahl;
    }

    return myFibo;

}

function printArray(erg){
    for (let i = 0; i < erg.length; i++) {
        document.write(erg[i] + ", "); 
    }
  
}

// --------------------

let myfibo = fillFibo(8);
printArray(myfibo);
~~~