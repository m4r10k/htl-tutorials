Schreibe ein Programm, welches einen Wert in einem Array sucht. Beispielwerte im Array: `"Mario", "Max", "Papa"....`
Der Benutzer soll einen zu suchenden Namen eingeben. Die Funktion zum suchen soll `findeImArray(meinArray)` lauten und `true` zurückgeben wenn der Name im Array gefunden wird und `false` wenn nicth.

Wird der Name gefunden, soll "Gefunden" ausgegeben werden, ansonst "Nicht gefunden".