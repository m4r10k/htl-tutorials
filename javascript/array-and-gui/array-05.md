# Aufgabe 5: Schreibe ein Programm, dass die Zahlen aus zwei Arrays je Feld zusammenzählt und die kleinste Zahl ermittelt

Es existieren bereits zwei Arrays bspw:

`a = [1,6,1,4]` und `b = [3,1,2,6]`

Diese Felder der beiden Arrays sollen Feldweise addiert werden und in ein weiteres Array gespeichert werden: bspw `1 + 3` = `4`. Die Zahl  `4` wird im neuen Array gespeichert.
Ermittle anschließend den kleinsten wert im neuen Array und gib diesen aus (return). Verwende dazu die Funktionen `addiereArrays(a, b, ergebnis)` `kleinsteFeldSumme(meinArray)`.

Bspw: `Die kleinste Feld-Summe = 3`

##### ++Aufgabe
Gib mit einer eigenen Funktion die Position zurück in dem der kleinste Wert im Array steht. In diesem Fall wäre das `2`.

-----

# Lösung:

~~~javascript
/**
 * File:    main.js
 * Project: 14_ja_while_schaltjahre
 * Author:  Johanna Kleinsasser
 * Date:    29.11.2018
 */

'use strict';

// functions
function addiereArrays(a, b, ergebnis){
    // Gehe über alle Felder von a
    // Addiere a an der Stelle i mit b an der Stelle i und merke das ergebnis in summe
    // pushe summe in das ergebnis array
    for (let i = 0; i < a.length; i++) {
        let sum = a[i] + b[i];
        ergebnis.push(sum);        
    }
}

function kleinsteFeldSumme(meinArray){
    let kleinsteZahl = meinArray[0];

    // Suche die kleinste Zahl
    for (let i = 0; i < meinArray.length; i++) {
        if (meinArray[i] < kleinsteZahl){
            kleinsteZahl = meinArray[i];
        }
        
    }

    // gib die kleinste Zhal zurück
    return kleinsteZahl;

}
// --------------------

let a = [1,6,1,4];
let b = [3,1,2,6];

let ergebnis = [];

addiereArrays(a, b, ergebnis);

// rufe die Funktion auf und speichere den Rückgabewert in kz
let kz = kleinsteFeldSumme(ergebnis);

document.write('Die kleinste Feld-Summe ist:' + kz); 
~~~