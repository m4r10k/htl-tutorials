Aufgabe 4: In einem Array sollen die 2 kleinsten Werte ermittelt werden.

Schreibe ein Programm, welches vom Benutzer solange positive Ganzzahlen einliest, bis dieser `0` eingibt.
Ermittle anschließend mit der Funktion `kleinsteZahlen(meinArray)` die zwei kleinsten Zahlen.

Bsp: Daten im Array `6,2,8,3,1,`
Ausgabe: `Die beiden kleinsten Zahlen sind 2 und 1.`


-----

# Lösung:

~~~javascript
/**
 * File:    main.js
 * Project: 14_ja_while_schaltjahre
 * Author:  Johanna Kleinsasser
 * Date:    29.11.2018
 */

'use strict';

// functions

function kleinsteZahlen(meinArray){

    // sortieren
    let getauscht = true;
    while(getauscht == true){
        getauscht = false;
        for (let i = 0; i < meinArray.length - 1; i++) {
            if(meinArray[i] > meinArray[i + 1]){
                let temp =  meinArray[i + 1];
                meinArray[i + 1] = meinArray[i];
                meinArray[i] = temp;
                getauscht = true;
            }     
        }
    }

    // Wenn fertig, gib die beiden kleinsten Zahlen aus
    document.write('Die beiden kleinsten Zahlen sind' + meinArray[0] + ' und ' + meinArray[1] + '.');
}

// ----------------



let number = 0;
let zahlen = [];

do{

    number = prompt('Gib eine Zahl ein:');
    number = Number(number);

    if (number > 0){
        zahlen.push(number);
    }

}while(number != 0);

// Rufe die Funktion auf
kleinsteZahlen(zahlen);
~~~