# Reverse words
Programmiere eine Oberfläche, welche es dem Benutzer erlaubt einen Satz einzugeben. Füge einen Button hinzu, welcher den Satz in umgekehrter Reihenfolge wieder ausgiebt.

## GUI
Eingabefeld `inputSentence`
Button `reverseInput`

## Bedingungen
Um den Satz in ein Array von Wörtern aufzuteilen verwende:

~~~
let arraySplittedSentence = inputSentence.split(' ');
~~~

### Wenn dies fertig ist:
Überprüft alle Eingaben auf Richtigkeit.
