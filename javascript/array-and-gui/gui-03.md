# Temperaturauswertung
Programmiere eine Oberfläche, welche es dem Benutzer erlaubt, sieben Werte einzugeben. (Montag bis Sonntag) Verwende dazu sieben Eingabefelder mit sieben Buttons. Füge zwei weitere Buttons hinzu: `Clear` und `Berechne`. Der `Clear` Button löscht alle Eingaben und leert das Array. Der Button `Berechne` zeigt den `niedrigsten` und `höchsten` Wert der Woche und den dazugehörigen Wochentag an. Dazu wird auch noch der Wochendurchschnitt angezeigt.


## GUI
Eingabefeld `temp1` Button `Speichern`

Eingabefeld `temp2` Button `Speichern`

Eingabefeld `temp3` Button `Speichern`

Eingabefeld `temp4` Button `Speichern`

Eingabefeld `temp5` Button `Speichern`

Eingabefeld `temp6` Button `Speichern`

Eingabefeld `temp7` Button `Speichern`

Button `Clear` Button `Berechne`

## Ausgabe
Tiefste Temperatur: `10` am `Montag`

Höchste Temperatur: `23` am `Sonntag`

Durchschnittliche Temperatur: `14` Grad


## Bedingungen
Verwende zur Implementierung die Funktion:
~~~
function lowestTemp(myArray)
function highestTemp(myArray)
function meanTemp(myArray)
function clearAllTemps(myArray)
function meanTemp(myArray)
function calculateTemps(myArray)
~~~

Überprüfe bei der Eingabe die Temperaturwerte: `nur Zahle sind erlaubt`, `niedrigster Wert -50`, `höchster Wert +50`.

### +++ Aufgabe
Mit jedem klick auf einen der `Speichern` Buttons wird die Berechnung durchgeführt und angezeigt.

