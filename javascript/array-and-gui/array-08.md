## Aufgabe 8: Welches beinhaltet die höhere Zahlensumme
Schreibe ein Programm, welches die Summe von jeweils zwei Zahlenarray berechnet und ausgibt, welches Array das größere ist.

ArrayA = `[1,2,25]` - Summe = 28
ArrayB = `[3,1,17,22]` - Summe = 41

Verwende dazu die Funktione `compareArrays(a,b)` welche zurückliefert, welches der übergebenen Arrays das größere ist. **Tipp**: Du kannst auch eine Hilfsfunktion mit dem Namen `summe(myArray)` verwenden.

-----

# Lösung:

~~~javascript

/**
 * File:    main.js
 * Project: 14_ja_while_schaltjahre
 * Author:  Johanna Kleinsasser
 * Date:    29.11.2018
 */

'use strict';

// functions
function compareArrays(a, b){
    let summeA = summe(a);
    let summeB = summe(b);

    if (summeA > summeB){
        return 'A';
    } else {
        return 'B';
    }

}

function summe(myArray){
    let sum = 0;
    for (let i = 0; i < myArray.length; i++) {
        sum = sum + myArray[i];
    }
    return sum;
}


// --------------------

let ArrayA = [1,2,25]
let ArrayB = [3,1,17,22] 

let ergebnis = compareArrays(ArrayA, ArrayB);

document.write('Array ' + ergebnis + ' ist größer'); 
~~~