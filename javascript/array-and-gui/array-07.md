## Aufgabe 7: Arrays zusammenfügen
Schreibe ein Programm, welches zwei Arrays zusammenfügt. Beispiel:

ArrayA = `[1,2,25]`
ArrayB = `[3,1,17,22]`

ErgebnisArray = `[1,2,25,3,1,17,22]`

Verwende dazu die Funktionen `mergeArrays(a, b)` welche ein das Ergebniarray **zurückliefert**  und `printArray(erg)` welche das Ergebnisarray ausgibt. **Beachte:** die erste Funktiont verwendet `return`!

-----

# Lösung:

~~~javascript

/**
 * File:    main.js
 * Project: 14_ja_while_schaltjahre
 * Author:  Johanna Kleinsasser
 * Date:    29.11.2018
 */

'use strict';

// functions
function mergeArrays(a, b){
    
    let myErgArray = [];

    for (let i = 0; i < a.length; i++) {
        myErgArray.push(a[i]);
    }

    for (let i = 0; i < b.length; i++) {
        myErgArray.push(b[i]);
    }

    return myErgArray

}

function printArray(erg){
    let sum = 0;
    for (let i = 0; i < erg.length; i++) {
        document.write(erg[i] + ", "); 
    }
  
}

// --------------------

let ArrayA = [1,2,25]
let ArrayB = [3,1,17,22] 

let ergebnis = mergeArrays(ArrayA, ArrayB);
printArray(ergebnis);

~~~
