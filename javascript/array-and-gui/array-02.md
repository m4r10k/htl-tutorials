Schreibe ein Programm, welches mit der Funktion `fuelleArray(meinArray)` ein Array mit den Werten von `0^2` bis `9^2` befüllt.
Gib danach die Werte des Array von `9^2` bis `0^2`, also `81, 64,...` mittels der Funktion `zeigeArray(meinarry)` wieder aus.
