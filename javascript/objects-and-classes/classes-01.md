# Der Schuhschrank

Wir wollen einen Schuhschrank programmieren, der Schuhe behinhaltet. Diese Schuhe können in den Schuhschrank gegeben werden und von dort wieder entnommen werden. Ein Schuhschrank hat nicht unendlich viel Kapazität, aber wir können mehrere Schuhschränke aufstellen.

Wir brauchen daher zwei Klassen: `ShoeCabinet` und `Shoe`. Alle Klassen-Eingenschaften (**properties**) sind mit **GETTER und SETTER** zu realiseren. **Hilfsfunktionen sind beliebig viele erlaubt.**

**Die Klassendiagramme** sind als Idee und **nicht als 1 zu 1 Vorlage zum Abschreiben** gedacht.

```mermaid

classDiagram
    class ShoeCabinet {
        slots
        inventory
        Name
        
        ShoeCabinet(slots)
        showInventory()
        addShoe(shoe)
        retrieveShoe(shoe)
        setName()
        getName()
    }

    class Shoe {
        color
        size
        type
        owner
        id
        setColor()
        getColor()
        setSize()
        getSize()
        setType()
        getType()
        setOwner()
        getOwner()
    }

```

Die Klasse `ShoeCabinet` verwaltet dabei eine Liste von Schuhen im `inventory`. Schuhe können hinzugefügt werden und aus dem Schuhkasten entnommen werden.

Das soll natürlich nur funktionieren, wenn die Schuhe:
- im Kasten sind
- der Kasten nicht voll ist
- die Schuhe nicht schon im Kasten sind 😂

## GUI

**PFLICHT:** Wir brauchen zur Dastellung des Schuhkasteninhalts eine GUI, die das Inventory anzeigt.

### Optional (Bonus)
Wir brauchen eine GUI zum:
- Anlegen des Schuhschranks
- Anlegen der Schuhe
- ... alle anderen Sinnvollen funktionen

## Tests

Test|Ergebnis|Check Papa
----|--------|----------
Kasten erzeugen | ✔ | 🆗
Schuh(e) erzeugen | ✔ | 🆗 
Dem Kasten die Schuhe hinzufügen | ✔ | 🆗
Einen Schuh entnehmen | ✔ | 🆗
Wieder hinzufügen | ✔ | 🆗 
Zuviel Schuhe dem Kasten hinzufügen | ❌
Zuviele Schuhe aus dem Kasten entnehmen | ❌
Den gleichen Schuh mehrmals entnehmen | ❌
Den gleichen Schuh mehrmals hinzufügen | ❌