![](http://www.commitstrip.com/wp-content/uploads/2016/08/Strip-Les-specs-cest-du-code-650-finalenglish.jpg)

# Project for additional HTL tutorials

Hier sind zusätzliche Aufgaben zum Thema Softwareentwicklung usw zu finden.

## Javascript

Der Order [Javascript](https://gitlab.com/m4r10k/htl-tutorials/tree/master/javascript) sind zusätzliche Bespiele für Javascript zu finden. Die Lösungen sind im Order `solutions` zu finden.